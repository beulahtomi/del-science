<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="../../../../favicon.ico">

      <title>DS | Hobby</title>
     <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
      
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <link href="assets/css/newstyle.css" rel="stylesheet">
  </head>
  <body>
    <header>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
              <strong>Home</strong>
          </a>
        </div>
      </div>
    </header>

  <main role="main">
    <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Hobby</h1>
            <p class="lead text-muted">Hobby is an activity done regularly in one's leisure time for pleasure. A hobby is also an activity, interest, enthusiasm, or pastime that is undertaken for pleasure or relaxation, done during one's own time.</p>
        <p>
          <a href="signup.php" class="btn btn-primary my-2">Sign Up</a>
          <a href="login.php" class="btn btn-secondary my-2">Log in</a>
        </p>
      </div>
    </section>
  </main>
      <nav class="navbar navbar-default navbar-inverse navbar-fixed-bottom">

       <footer class="mastfoot mt-auto">
            <div class="inner">
              <center><p>Hobby, by <a href="https://twitter.com/Tomi_ni_mi?s=08">@tomi</a>.</p></center>
            </div>
      </footer>
 </div>
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      <script src="../../assets/js/vendor/popper.min.js"></script>
      <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>