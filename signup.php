<?php
  session_start();
  
  if (isset($_SESSION['user_id']) || isset($_SESSION['username']) || isset($_SESSION['email'])) {
    header('Location: hobby.php');
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../../../../favicon.ico">
    <title>DS | Hobby</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="assets/css/newstyle.css" rel="stylesheet">
  </head>

<body>
   <header>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Sign Up</strong>
            <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="Index.php" style="color:white">Home</a>
            <a class="nav-link" href="login.php" style="color:white">Log In</a>
            </nav>
          </a>
        </div>
      </div>
    </header>
   
      <main role="main" class="inner cover">
          <h1 class="cover-heading">Sign Up</h1>
              <br>
  <div>
    <form  action="" method="post">
      	<div class="form-group row">
      		<label class="control-label" style="font-weight: bold;">First Name</label>
              <input type="text" class="form-control" name="first_name" placeholder="First name" required >
      </div>

      <div class="form-group row">
      	<label class="control-label" style="font-weight: bold;">Last Name</label>
            <input type="text" class="form-control" name="last_name" placeholder="Last name" required >
      </div>
      	
      <div class="form-group row">
      		<label class="control-label" style="font-weight: bold;">Email</label>
            <input type="email" class="form-control" name="email" placeholder="email" required >
      </div>

      <div class="form-group row">
      		<label class="control-label" style="font-weight: bold;"> Username</label>
              <input type="text" class="form-control" name="username" placeholder="Username" required >
      </div>

      <div class="form-group row">
      		<label class="control-label" style="font-weight: bold;"> Password</label>
              <input type="password" class="form-control" name="password" placeholder="Password" required >
      </div>
  	
  	
      <input type="submit" name="submit" class="btn btn-md btn-default" value="Submit">
  	</form>
  </div>
        
       </div>
      </section>
    </main>

  <nav class="navbar navbar-default navbar-inverse navbar-fixed-bottom">
   <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Hobby, by <a href="https://twitter.com/Tomi_ni_mi?s=08">@tomi</a>.</p>
        </div>
    </footer>
  </nav>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
  </body>
</html>
<?php 
if(isset($_POST['submit'])){
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$email=$_POST['email'];
	$user=$_POST['username'];
	$pass=$_POST['password'];
	
    $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
    
    $server='us-cdbr-iron-east-01.cleardb.net';
    $username='bf26a10317fc8e';
    $password='2f0276a7';
    $db='heroku_f5da41798de4a92';
    $conn = new mysqli($server,$username,$password,$db);

	if($conn->connect_error){
		die("connection failed: " . $conn->connect_error);	
	}
	$sql="INSERT into user_table(first_name,last_name,email,username,password) values('$first_name','$last_name','$email','$user','$pass')";
	if($conn->query($sql)===TRUE){
		$last_id = $conn->insert_id;
		header( 'Location: login.php');
	}
	else{
	 	echo "didnt work";
 	}
}
?>

