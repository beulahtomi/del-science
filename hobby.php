<?php
    session_start();
    $id = $_SESSION['user_id'];

$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
    $server='us-cdbr-iron-east-01.cleardb.net';
    $username='bf26a10317fc8e';
    $password='2f0276a7';
    $db='heroku_f5da41798de4a92';

    $conn = new mysqli($server,$username,$password,$db);

    if($conn->connect_error){
        die("connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM `hobby_tbl` WHERE user_id = $id";

    $result = mysqli_query($conn, $sql);

    $saved_hobbies = array();

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            array_push($saved_hobbies, $row);
        }
    }
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>DS | Hobby</title>

    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="assets/css/newstyle.css" rel="stylesheet">
  </head>

  <body>
    <header>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Hobby List</strong>
            <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="Index.php" style="color:white">Home</a>
            <a class="nav-link" href="logout.php" style="color:white">Log out</a>
            </nav>
          </a>
        </div>
      </div>
    </header>
<div class="container jumbotron">
    <main role="main" class="inner cover" >
      <center><h1 class="cover-heading">Hobby List</h1></center>
          <div class="row">
            <?php
                foreach($saved_hobbies as $hobby)
                {
                    
                    echo "<div class='col-md-6'><div class='card' style='color:black'>" . $hobby['name']. "</div></div>";
                    echo "<br>";
                }   
            ?> 
          </div>
        <p class="lead">
        <center><a href="home.php" class="btn btn-lg btn-secondary">Add Hobby</a></center>
        </p>
        </div>
    </main>

    <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Hobby, by <a href="https://twitter.com/Tomi_ni_mi?s=08">@tomi</a>.</p>
        </div>
    </footer>
</div>

      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      <script src="../../assets/js/vendor/popper.min.js"></script>
      <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>


